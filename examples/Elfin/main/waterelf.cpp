// waterelf.cpp

#include "waterelf.h"

uint8_t BikeTyreValve::counter = 0; // counter for setting valve number

BikeTyreValve::BikeTyreValve() {
  number = counter++;
}

void BikeTyreValve::open() {
  digitalWrite(pumpPin, LOW);
  digitalWrite(solPin, HIGH);

  valveIsOpen = true;
}

void BikeTyreValve::close() {
  digitalWrite(pumpPin, HIGH);
  digitalWrite(solPin, LOW);

  valveIsOpen = false;
}

const uint8_t WaterElf::NUM_GROW_BEDS;           // = number of valves etc.
constexpr int WaterElf::pumpPins[NUM_GROW_BEDS]; // pump pins
constexpr int WaterElf::solPins[NUM_GROW_BEDS];  // solenoid pins
const unsigned long FLOOD_DURATION = 15 * 60 * 1000; // flood time, millisecs

WaterElf::WaterElf() {
  // initialise the growbed valves
  for(int i = 0; i < NUM_GROW_BEDS; i++) {
// TODO new the valves, with pin/pin constructor
    pinMode(pumpPins[i], OUTPUT);    // set up the...
    pinMode(solPins[i], OUTPUT);     // ...gpio pins

    valves[i].pumpPin = pumpPins[i]; // tell the valve...
    valves[i].solPin = solPins[i];   // ...what pins it is on
    valves[i].open();                // leave it open
  }
}

void WaterElf::start() {
  floodingValve = valves[valveCursor++];
  floodingValve.close();
  lastStep = millis();
}

void WaterElf::step() {
  unsigned long now = millis();
  if(now - lastStep > FLOOD_DURATION) {
    floodingValve.open();
    if(valveCursor++ == NUM_GROW_BEDS)
      valveCursor = 0;
    floodingValve = valves[valveCursor];
    floodingValve.close();
    lastStep = millis();
  }
}

void WaterElf::blinkTest() {
  Serial.println("waiting 2 secs...");
  delay(2000);

  for(int i = 0; i < NUM_GROW_BEDS; i++) {
    digitalWrite(pumpPins[i], HIGH); // pump alone
    delay(5000);
    digitalWrite(pumpPins[i], LOW);
    delay(300);

    digitalWrite(solPins[i], HIGH);  // solenoid alone
    delay(5000);
    digitalWrite(solPins[i], LOW);
    delay(300);

    digitalWrite(pumpPins[i], HIGH); // both
    digitalWrite(solPins[i], HIGH);
    delay(5000);
    digitalWrite(pumpPins[i], LOW);
    digitalWrite(solPins[i], LOW);
    delay(300);
  }

  Serial.println("waiting 2 secs...");
  delay(2000);
}

void WaterElf::blinkFeatherLED(uint8_t times) {
  for(int i=0; i<times; i++) {
    digitalWrite(BUILTIN_LED, LOW); delay(300);
    digitalWrite(BUILTIN_LED, HIGH); delay(300);
  }
}
