// waterelf.h 
// definitions particular to the WaterElf

#ifndef WATERELF_H
#define WATERELF_H

#include "unphelf.h"         // definitions shared with unphone

class BikeTyreValve {
 public:
  BikeTyreValve();
  void open();
  void close();
  bool isOpen() { return valveIsOpen; }

  static uint8_t counter;  // counter for setting valve number
  uint8_t number;          // id of this valve, counting from 1
  uint8_t pumpPin;
  uint8_t solPin;
  bool valveIsOpen = true; // current state
};

class WaterElf {
 public:
  WaterElf();
  void blinkTest();
  void blinkFeatherLED(uint8_t times);
  void start();
  void step();

  static const uint8_t NUM_GROW_BEDS = 3; // how many beds (and valves)?
  static constexpr int pumpPins[NUM_GROW_BEDS] = { 32, 27, A0 }; // pumps
  static constexpr int solPins[NUM_GROW_BEDS]  = { 21, 15, A5 }; // solenoids

  BikeTyreValve valves[NUM_GROW_BEDS];  // the valves
  uint8_t valveCursor = 0;              // index of flood bed valve
  BikeTyreValve& floodingValve = valves[0]; // valve of currently flooding bed
  unsigned long lastStep;               // last time we changed flooding bed
  static const unsigned long FLOOD_DURATION = 15 * /* TODO 60 * */ 1000; // in milisecs
};

#endif
