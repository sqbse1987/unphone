// derived from Adafruit_NeoPixel/examples/simple/simple.ino at
// https://github.com/adafruit/Adafruit_NeoPixel/blob/master/examples/simple/simple.ino
// thanks!

// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            A7 // = GPIO 32

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      32

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

int delayval = 50; // delay for few millis

void setup() {
  pixels.begin(); // This initializes the NeoPixel library.
}

void loop() {
  // clear the array
  for(int i=0; i<NUMPIXELS; i++)
    pixels.setPixelColor(i, pixels.Color(0,0,0));
  pixels.show();

  // For a set of NeoPixels the first NeoPixel is 0, second is 1, all the way up to the count of pixels minus one.
  for(int i=0; i<NUMPIXELS; i++) {
    // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
    pixels.setPixelColor(i, pixels.Color(0,100,0)); // Moderately bright green color.
    if(i > 0) pixels.setPixelColor(i - 1, pixels.Color(0,0,0));
    pixels.show(); // This sends the updated pixel color to the hardware.
    delay(delayval); // Delay for a period of time (in milliseconds).
  }
  for(int i= NUMPIXELS - 1; i>=0; i--) {
    pixels.setPixelColor(i, pixels.Color(100,0,0));
    if(i < NUMPIXELS) pixels.setPixelColor(i + 1, pixels.Color(0,0,0));
    pixels.show(); // This sends the updated pixel color to the hardware.
    delay(delayval); // Delay for a period of time (in milliseconds).
  }
  for(int i=0; i<NUMPIXELS; i++) {
    pixels.setPixelColor(i, pixels.Color(0,0,100));
    if(i > 0) pixels.setPixelColor(i - 1, pixels.Color(0,0,0));
    pixels.show(); // This sends the updated pixel color to the hardware.
    delay(delayval); // Delay for a period of time (in milliseconds).
  }
  for(int i= NUMPIXELS - 1; i>=0; i--) {
    pixels.setPixelColor(i, pixels.Color(100,50,0));
    if(i < NUMPIXELS) pixels.setPixelColor(i + 1, pixels.Color(0,0,0));
    pixels.show(); // This sends the updated pixel color to the hardware.
    delay(delayval); // Delay for a period of time (in milliseconds).
  }
}
