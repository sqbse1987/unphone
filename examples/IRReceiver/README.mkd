# Infra-Red Receiver

This project was an adjunct to an IR blaster project to use the unphone as a
universal IR remote control. The reciever allowed students to check on the
performance of their project - without having to carry a giant TV around.

The IR remote project was minimal in terms of additional electronics, but
challenging in that the world of IR codes is a complex, decades old mix of
propriatary standards, evolution beyond those standards, and patchy
documentation from dozens of projects. Students who didn't have an existing
remote control to read codes from were given a deep immersion into the art of
penetrating forum posts and code comments to gleam information.
