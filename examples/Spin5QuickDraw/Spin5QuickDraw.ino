// Spin5QuickDraw.ino from Gee's original
// self-contained example sketch illustrating quick text drawing on the LCD

#include "lmic.h"
#include "hal/hal.h"
#include <SPI.h>
#include "Adafruit_GFX.h"       // core graphics library
#include "Adafruit_HX8357.h"    // tft display local hacked version
#include "Adafruit_STMPE610.h"  // touch screen local hacked version
#include <Wire.h>
#include "IOExpander.h"
#include "chunk.h"
#include "Adafruit_Sensor.h"
#include "Adafruit_LSM303_U.h"

#define VBAT_SENSE 35

/* Assign a unique ID to accel sensor at the same time */
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(54321);

//#define TFT_DC   33
Adafruit_HX8357 tft = Adafruit_HX8357(IOExpander::LCD_CS, IOExpander::LCD_DC, IOExpander::LCD_RESET);
Adafruit_STMPE610 ts = Adafruit_STMPE610(IOExpander::TOUCH_CS);

// calibration data for the raw touch data to the screen coordinates
#define TS_MINX 3800
#define TS_MAXX 100
#define TS_MINY 100
#define TS_MAXY 3750
#define LORA_APP_KEY { 0xF3, 0xEA, 0x3B, 0x0B, 0xCD, 0xF2, 0xD0, 0xB9, 0x53, 0x69, 0xA6, 0x5F, 0x08, 0x14, 0x70, 0x27 }
#define LORA_NET_KEY { 0xFA, 0x31, 0x8A, 0x18, 0x7F, 0x67, 0x12, 0x67, 0xFC, 0xF2, 0x46, 0x8C, 0x44, 0xA9, 0xDA, 0xF3 }
#define LORA_DEV_ADDR 0x2601184A
#define LORA_NAME "testdev2"

// a test set of boxes
class Menu: public Chunk {
public:
  void init() {
    Wire.setClock(400000);
    Wire.begin();
  }
};
Menu m;

byte BM_I2Cadd = 0x6b;
byte BM_Status   = 0x08; // System Status Register 
byte BM_Version  = 0x0a; // Vender / Part / Revision Status Register 

#include "SD.h"

void do_send(osjob_t*);
void write8(byte address, byte reg, byte value);        // ...helpers
byte read8(byte address, byte reg);                     // 
// Pin mapping LORA STUFFFFFF
// LoRaWAN NwkSKey, network session key
// This is the default Semtech key, which is used by the prototype TTN
// network initially.
static const PROGMEM u1_t NWKSKEY[16] = LORA_NET_KEY;

// LoRaWAN AppSKey, application session key
// This is the default Semtech key, which is used by the prototype TTN
// network initially.
static const u1_t PROGMEM APPSKEY[16] = LORA_APP_KEY;

// LoRaWAN end-device address (DevAddr)
// See http://thethingsnetwork.org/wiki/AddressSpace
static const u4_t DEVADDR = LORA_DEV_ADDR ; // <-- Change this address for every node!

// These callbacks are only used in over-the-air activation, so they are
// left empty here (we cannot leave them out completely unless
// DISABLE_JOIN is set in config.h, otherwise the linker will complain).
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }

static uint8_t mydata[] = "Hello, feathery world!";
static osjob_t sendjob;
static osjob_t voltage_update_job;
static osjob_t os_details_job;
// Schedule TX every this many seconds (might become longer due to duty
// cycle limitations).
const unsigned TX_INTERVAL = 20;

void onEvent (ev_t ev) {
    Serial.print(os_getTime());
    Serial.print(": ");
    switch(ev) {
        case EV_SCAN_TIMEOUT:
            Serial.println(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_BEACON_FOUND:
            Serial.println(F("EV_BEACON_FOUND"));
            break;
        case EV_BEACON_MISSED:
            Serial.println(F("EV_BEACON_MISSED"));
            break;
        case EV_BEACON_TRACKED:
            Serial.println(F("EV_BEACON_TRACKED"));
            break;
        case EV_JOINING:
            Serial.println(F("EV_JOINING"));
            break;
        case EV_JOINED:
            Serial.println("netid = ");
            Serial.println(F("EV_JOINED"));
            break;
        case EV_RFU1:
            Serial.println(F("EV_RFU1"));
            break;
        case EV_JOIN_FAILED:
            Serial.println(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            Serial.println(F("EV_REJOIN_FAILED"));
            break;
            break;
        case EV_TXCOMPLETE:
            
            Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
                tft.setCursor(0,40);
                tft.fillRect(0, 40, 100, 24 , HX8357_BLACK);
                tft.setTextSize(3);
                tft.setTextColor(HX8357_GREEN);
                tft.println("Packet Sent:");
            if(LMIC.dataLen) {
                // data received in rx slot after tx
                Serial.print(F("Data Received: "));
                Serial.write(LMIC.frame+LMIC.dataBeg, LMIC.dataLen);
                Serial.println();
            }
            // Schedule next transmission
            os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
           
            break;
        case EV_LOST_TSYNC:
            Serial.println(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            Serial.println(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            Serial.println(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            Serial.println(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            Serial.println(F("EV_LINK_ALIVE"));
            break;
         default:
            Serial.println(F("Unknown event"));
            break;
    }
}

//////////////////////////////////////OS JOBS////////////////////////////////////////
void do_send(osjob_t* j){
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) {
        Serial.println(F("OP_TXRXPEND, not sending"));
    } else {
        // Prepare upstream data transmission at the next possible time.
        LMIC_setTxData2(1, mydata, sizeof(mydata)-1, 0);
        Serial.println(F("Packet queued"));
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

void do_voltage_update(osjob_t * j){
  float voltage;
  voltage = analogRead(VBAT_SENSE);
  //Serial.println(voltage_old - voltage_new);

  voltage = (voltage/ 4095) * 4.4;
  tft.setCursor(200,0);
  tft.fillRect(200, 0, 100, 24 , HX8357_BLACK);
  tft.setTextSize(3);
  tft.setTextColor(HX8357_GREEN);
  IOExpander::digitalWrite(IOExpander::BACKLIGHT,HIGH);
  Serial.println(voltage);
  tft.println(voltage);
  os_setTimedCallback(&voltage_update_job, os_getTime()+ms2osticks(500), do_voltage_update);
}

void do_os_details(osjob_t *j){
  Serial.println( os_getBattLevel());
  Serial.println(os_getTime());
  os_setTimedCallback(&os_details_job, os_getTime()+sec2osticks(1), do_os_details);
}
const lmic_pinmap lmic_pins = {
  .nss = IOExpander::LORA_CS,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = IOExpander::LORA_RESET,
  .dio = {39,26,LMIC_UNUSED_PIN}  // Modified to suit our connection on unphone
};

int stage=1;
bool newSwitchReading, oldSwitchReading;

float voltage;
float voltage_old;
float voltage_new; 

void setup() {
  Serial.begin(115200);
  if(!accel.begin()) {
    // There was a problem detecting the ADXL345 ... check your connections
    Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
    while(1);
  }
  pinMode( VBAT_SENSE,INPUT);
  analogReadResolution(12);  // 10 bit is 0-1023, 11 bit is 0-2047, 12 bit is 0-4095
  analogSetPinAttenuation( VBAT_SENSE, ADC_6db); // 0db is 0-1V, 2.5db is 0-1.5V, 6db is 0-2.2v, 11db is 0-3.3v
  m.init();
  sensor_t sensor;
  accel.getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" m/s^2");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" m/s^2");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" m/s^2");
  Serial.println("------------------------------------");
  Serial.println("");

  IOExpander::begin();
  oldSwitchReading=IOExpander::digitalRead(IOExpander::POWER_SWITCH);
  IOExpander::digitalWrite(IOExpander::BACKLIGHT,HIGH);
  tft.begin(HX8357D);
  tft.fillScreen(HX8357_BLACK);
  tft.setCursor(0,0);
  tft.println(LORA_NAME);
  tft.setTextColor(HX8357_GREEN);

  tft.fillScreen(HX8357_BLACK);
  tft.setTextSize(3);
  tft.setCursor(120,10);
  tft.setTextColor(HX8357_RED);
  tft.print("Red");  
  tft.setCursor(120,40);
  tft.setTextColor(HX8357_GREEN);
  tft.print("Green");
  tft.setCursor(120,70);
  tft.setTextColor(HX8357_BLUE);
  tft.print("Blue");
  tft.fillRect(30, 30, 70, 70 , HX8357_GREEN);
  tft.fillRect(220, 30, 70, 70 , HX8357_RED);
  tft.fillRect(30, 380, 70, 70 , HX8357_BLUE);
  tft.fillRect(220, 380, 70, 70 , HX8357_YELLOW);
  
  IOExpander::digitalWrite(IOExpander::BACKLIGHT,HIGH);
  tft.setCursor(0,110);
  tft.setTextColor(HX8357_GREEN);

  if(! ts.begin()) {
    D("failed to start touchscreen controller");
    tft.setTextColor(HX8357_RED);
    tft.println(" TOUCH INIT FAIL");
    tft.setTextColor(HX8357_GREEN);
  } else {
    D("touchscreen started");
    tft.println(" TOUCH INIT OK");
  }
  
  // LMIC init
  Serial.println("doing os_init()...");
  tft.setCursor(0,206);
  tft.setTextColor(HX8357_RED);   
  tft.println(" LORA INIT FAIL");
  os_init(); // if lora fails then will stop at this init, allowing fail to be seen
  tft.setCursor(0,206);
  tft.fillRect(0, 206, 319, 24 , HX8357_BLACK);
  tft.setTextColor(HX8357_GREEN);   
  tft.println(" LORA INIT OK");
  LMIC_reset();

  // Set static session parameters. Instead of dynamically establishing a session
  // by joining the network, precomputed session parameters are be provided.
  #ifdef PROGMEM
  // On AVR, these values are stored in flash and only copied to RAM
  // once. Copy them to a temporary buffer here, LMIC_setSession will
  // copy them into a buffer of its own again.
  uint8_t appskey[sizeof(APPSKEY)];
  uint8_t nwkskey[sizeof(NWKSKEY)];
  memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
  memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
  LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
  #else
  // If not running an AVR with PROGMEM, just use the arrays directly 
  LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
  #endif

  // Set up the channels used by the Things Network, which corresponds
  // to the defaults of most gateways. Without this, only three base
  // channels from the LoRaWAN specification are used, which certainly
  // works, so it is good for debugging, but can overload those
  // frequencies, so be sure to configure the full frequency range of
  // your network here (unless your network autoconfigures them).
  // Setting up channels should happen after LMIC_setSession, as that
  // configures the minimal channel set.
  LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
  LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK,  DR_FSK),  BAND_MILLI);      // g2-band
  // TTN defines an additional channel at 869.525Mhz using SF9 for class B
  // devices' ping slots. LMIC does not have an easy way to define set this
  // frequency and support for class B is spotty and untested, so this
  // frequency is not configured here.

  // Disable link check validation
  LMIC_setLinkCheckMode(1);

  // Set data rate and transmit power (note: txpow seems to be ignored by the library)
  LMIC_setDrTxpow(DR_SF7,14);
  do_send(&sendjob);
  //do_voltage_update(&voltage_update_job);
  //do_os_details(&os_details_job);
}

void loop(void) {
  os_runloop_once();
  //os_runloop();

  //all of the below is now rgestered as an os job 
  //int count;
  sensors_event_t event;
  accel.getEvent(&event);

  /* Display the results (acceleration is measured in m/s^2) */
  Serial.print("X: "); Serial.print(event.acceleration.x); Serial.print("  ");
  Serial.print("Y: "); Serial.print(event.acceleration.y); Serial.print("  ");
  Serial.print("Z: "); Serial.print(event.acceleration.z); Serial.print("  ");Serial.println("m/s^2 ");
 
  voltage_new = analogRead(25);
  Serial.println(voltage_old - voltage_new);
  if (voltage_old - voltage_new > 3){
    voltage_old = voltage_new;
    voltage = (voltage_old/ 4095) * 4.4;
    tft.setCursor(200,0);
    tft.fillRect(200, 0, 100, 24 , HX8357_BLACK);
    tft.setTextSize(3);
    tft.setTextColor(HX8357_GREEN);
    IOExpander::digitalWrite(IOExpander::BACKLIGHT,HIGH);
  
    tft.println(voltage);
  }
  else {
    //IOExpander::digitalWrite(IOExpander::BACKLIGHT,LOW);
  }
  delay(200);
  voltage_old = voltage_new;
}

void setRegister(byte address, byte reg, byte value) {
  write8(address, reg, value);
}

byte getRegister(byte address, byte reg) {
  byte result;
  result=read8(address,reg);
  return result;
}

void write8(byte address, byte reg, byte value) {
  Wire.beginTransmission(address);
  Wire.write((uint8_t)reg);
  Wire.write((uint8_t)value);
  Wire.endTransmission();
}

byte read8(byte address, byte reg) {
  byte value;
  Wire.beginTransmission(address);
  Wire.write((uint8_t)reg);
  Wire.endTransmission();
  Wire.requestFrom(address, (byte)1);
  value = Wire.read();
  Wire.endTransmission();
  return value;
}
