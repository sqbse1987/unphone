## Welcome to WaterElf
### A helpful device for monitoring and controlling aquaponics systems.

#### Water Temperature Sensor
The water temperature sensor is fully submersible and should be placed a few cm into the fish tank somewhere out of the way.

![](waterproof-ds18b20.jpg)
![](watertempsensor.png)

Internally, it is a DS18B20 1-wire temperature sensor [datasheet](https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf), packaged in a stainless steel tube and sealed against water. It has three connections, typically coloured black, red and yellow for ground, power and data respectively.

The waterelf connector for this sensor is a three pin type SP-13, with numbered pins on the plugs and sockets themselves. The diagram below is looking into the face of the sensor plug.

![](SP13-3pin-connector.png)

| Pin | Function     | Cable colour |
|-----|--------------|--------------|
| 1   | Ground       | Black        | 
| 2   | Power (3.3V) | Red          | 
| 3   | Data         | Yellow       |
